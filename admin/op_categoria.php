<?php
require_once("conexao.php");
$txt_categoria = $_POST['txt_categoria'];
$txt_ativo = $_POST['txt_ativo'];
$sql = "INSERT INTO categoria (categoria, cat_ativo) VALUES (:cat,:atv)";
 
try {
        $cmd = $conn->prepare($sql);
        $cmd->execute(array(
            ':cat'=>$txt_categoria,
            ':atv'=>$txt_ativo
        ));
        print "<script type='text/javascript'>location.href='principal.php?link='</script>";
    } catch (PDOException $ex) {
        echo 'Erro: '.$ex->getMessage();
        
    }
    ?>