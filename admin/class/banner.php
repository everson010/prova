<?php

class banner{
    $id;
    $titulo_banner;
    $link_banner;
    $img_banner;
    $alt;
    $banner_ativo;
}

public static function loadById($banner){
        $sql = new $sql();
        $results = $sql->select("SELECT * FROM administrador WHERE banner = :banner",array(":id"=>$banner));
        if (count($results)>0) {
            $this->SetData($results[0]);
        } 
    }
    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM administrador ORDER BY banner");
    }

    public static function search($banner){
        $sql = new Sql();
        return $sql->select("SELECT * FROM  administrador WHERE banner LIKE :banner", array(":banner"=>"%".$banner."%"));
?>

