<?php
//require_once('../config.php');
//  `id` int(11) NOT NULL,
//  `nome` varchar(200) NOT NULL,
//  `email` varchar(200) NOT NULL,
//  `login` varchar(100) NOT NULL,
//  `senha` varchar(50) NOT NULL

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulário ADMIN</title>
    <link rel="stylesheet" href="../css/style_admin.css">
</head>
<body>
<div id="formulario-menor">
    <form action="op_administrador.php" method="post">
        <fieldset>
            <input type="hidden" id="id" name="id">
            <label for="">Nome</label>
            <input type="text" name="nome" required placeholder="Digite o nome">
            <p>
            <label for="">Email</label>
            <input type="text" name="email" required placeholder="Digite o email">
            <p>
            <label for="">Login</label>
            <input type="text" name="login" required placeholder="Digite o login">
            <p>
            <label for="">Senha</label>
            <input type="password" name="senha" required placeholder="Digite o senha">
            <p>
            <label for="">Confima Senha</label>
            <input type="password" name="confirma_senha" required placeholder="Digite novamente a senha">
            <p>
            <input type="submit" name="cadastro" value="Cadastrar Administrador">
        </fieldset>
    </form>
</div>
    
</body>
</html>
