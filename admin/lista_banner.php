<?php
require_once('conexao.php');
 $query = "select * from banner";
 $cmd = $conn->prepare($query);
 $cmd->execute();
 $banner_retornado = $cmd->fetchAll(PDO::FETCH_ASSOC);
 if (count($banner_retornado)>0) {
   

    //var_dump($banner_retornado);

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listagem banner</title>
    <link rel="stylesheet" href="css/style_admin.css">
</head>
<body>
    <table width="100%" border="0" cellpadding = "0" cellspacing="1" bgcolor="#660000">
        <tr bgcolor="#993300" align="center">
            <td width="15%" heigth="2"><strong><font size="2" color="#fff">Código</font></strong></td>
            <td width="60%" heigth="20"><strong><font size="2" color="#fff">banner</font></strong></td>
            <td width="15%" heigth="20"><strong><font size="2" color="#fff">Ativo</font></strong></td>
            <td colspan="2" heigth="20"><strong><font size="2" color="#fff">Opcões</font></strong></td>
        </tr>
    <?php 
      foreach ($banner_retornado as $banner) {
        
    ?>
        <tr bgcolor="#fff">

            <td><font size="2" face="verdana,arial"><?php echo $banner["id_banner"]; ?></font></td>
            <td><font size="2" face="verdana,arial"><?php echo $banner["titulo_banner"]; ?></font></td>
            <td><font size="2" face="verdana,arial"><?php echo $banner["link_banner"]; ?></font></td>
            <td><font size="2" face="verdana,arial"><?php echo $banner["img_banner"]; ?></font></td>
            <td><font size="2" face="verdana,arial"><?php echo $banner["alt"]; ?></font></td>
            <td><font size="2" face="verdana,arial"><?php echo $banner["banner_ativo"]; ?></font></td>
            <td align="center"><font size="2" face="verdana,arial"><a href="principal.php?link=8">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana,arial"><a href="principal.php?link=9">Excluir</a></font></td>
    <?php   }} ?>
        </tr>
    </table>
</body>
</html>

